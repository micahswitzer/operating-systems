#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#define STR_LEN 100

void invert_case(char*, int);

int main(int argc,char **argv)
{
	char str[STR_LEN];
	int listen_fd, comm_fd;

	struct sockaddr_in serveraddr;

	listen_fd = socket(AF_INET, SOCK_STREAM, 0);

	bzero(&serveraddr, sizeof(serveraddr));

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htons(INADDR_ANY);
	serveraddr.sin_port = htons(5656);

	bind(listen_fd, (struct sockaddr *) &serveraddr, sizeof(serveraddr));

	listen(listen_fd, 10);

	while (1)
	{
		bzero(str, STR_LEN);
		comm_fd = accept(listen_fd, (struct sockaddr*) NULL, NULL);
		read(comm_fd, str, STR_LEN);
		printf("Received data: %s\n", str);
		invert_case(str, STR_LEN);
		write(comm_fd, str, strlen(str) + 1);
		close(comm_fd);
	}
}

void invert_case(char *str, int len)
{
	for (int i = 0; i < len; i++)
	{
		if (str[i] == 0) return;
		if (str[i] > 0x40 && 0x5B > str[i])
			str[i] += 0x20;
		else if (str[i] > 0x60 && 0x7B > str[i])
			str[i] -= 0x20;
	}
}
